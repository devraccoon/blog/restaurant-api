import mongoengine as me

class Restaurant(me.Document):
    URL = me.StringField()
    address = me.StringField()
    address_line_2 = me.StringField(db_field="address line 2")
    name = me.StringField()
    outcode = me.StringField()
    postcode = me.StringField()
    rating = me.IntField()
    type_of_food = me.StringField()
