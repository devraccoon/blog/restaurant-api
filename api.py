from flask import Flask, request
from mongoengine import connect
from bson.json_util import dumps
from models import Restaurant

app = Flask(__name__)


def aggregate_from_request(request, stage_specifications):
    args = set(request.args)
    aggregate_stages = [
        query_builder(request) for required_fields, query_builder
        in stage_specifications if args >= required_fields
    ]
    return aggregate_stages


def create_limit_stage(request, default_limit=50):
    page_limit = int(request.args.get('page_limit', default_limit))
    return {'$limit': page_limit}


def create_skip_stage(request):
    page_limit = int(request.args.get('page_limit'))
    current_page = int(request.args.get('current_page'))
    return {'$skip': page_limit*current_page}


def create_search_stage(request):
    search_term = request.args.get('search')
    fields = ['name', 'address', 'type_of_food', 'rating']
    return ({
        '$match': {
            '$or': [
                {
                    field:
                    {
                        '$regex': search_term,
                        '$options': 'i',
                    }
                }
                for field in fields
            ]
        }
    })


def parse_sort_signal(field):
    if field[0] == '-':
        return -1
    return 1


def parse_sort_fields(fields):
    return {field[1:]: parse_sort_signal(field)
            for field in fields}


def create_sort_stage(request):
    sort_fields = request.args.getlist('sort')
    return {'$sort': parse_sort_fields(sort_fields)}


@app.route("/api/restaurants", methods=['GET'])
def get_restaurants():
    connect('poc')
    stage_specifications = [
        ({'search'}, create_search_stage),
        ({'sort'}, create_sort_stage),
        ({'page_limit', 'current_page'}, create_skip_stage),
        (set(), create_limit_stage)
    ]
    aggregate_stages = aggregate_from_request(request, stage_specifications)
    restaurants = list(Restaurant.objects.aggregate(*aggregate_stages))
    return dumps({
        "count": len(restaurants),
        "data": restaurants,
    })


app.run(debug=True)
